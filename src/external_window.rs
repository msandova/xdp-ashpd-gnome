use ashpd::WindowIdentifierType;
use gtk::glib;
use gtk::prelude::*;
use std::os::raw::c_ulong;

use crate::external_wayland_window::ExternalWaylandWindow;
use crate::external_x11_window::ExternalX11Window;

pub(crate) fn set_foreign_parent<W: glib::IsA<gtk::Window>>(
    window: &W,
    window_identifier: WindowIdentifierType,
) {
    match window_identifier {
        WindowIdentifierType::Wayland(exported_handle) => do_it_wayland(window, exported_handle),
        WindowIdentifierType::X11(foreign_xid) => do_it_x11(window, foreign_xid),
    };
}

fn do_it_wayland<W: glib::IsA<gtk::Window>>(window: &W, exported_handle: String) -> Option<()> {
    let external = ExternalWaylandWindow::new(exported_handle)?;
    let display = &external.wl_display;
    let fake_parent = gtk::Window::new();
    fake_parent.set_display(display);
    window.set_transient_for(Some(&fake_parent));
    drop(fake_parent);
    gtk::Widget::realize(window.as_ref().upcast_ref());
    external.set_parent_of(
        window
            .as_ref()
            .surface()
            .downcast_ref::<gdk_wayland::WaylandSurface>()?,
    );

    Some(())
}

fn do_it_x11<W: glib::IsA<gtk::Window>>(window: &W, foreign_xid: c_ulong) -> Option<()> {
    let external = ExternalX11Window::new(foreign_xid)?;
    let fake_parent = gtk::Window::new();
    // TODO This does not work, we segfault if we set this as the display.
    // let display = &external.x11_display;
    // fake_parent.set_display(display);
    window.set_transient_for(Some(&fake_parent));
    drop(fake_parent);
    gtk::Widget::realize(window.as_ref().upcast_ref());
    external.set_parent_of(
        window
            .as_ref()
            .surface()
            .downcast_ref::<gdk_x11::X11Surface>()?,
    );

    Some(())
}
