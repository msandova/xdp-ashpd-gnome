use ashpd::backend::{
    FileChooserImpl, OpenFileOptions, OpenFileResults, RequestImpl, SaveFileOptions,
    SaveFileResults, SaveFilesOptions, SaveFilesResults,
};
use ashpd::desktop::file_chooser::Choice;
use ashpd::desktop::file_chooser::FileFilter;
use ashpd::desktop::Response;
use ashpd::AppID;
use ashpd::WindowIdentifierType;
use async_trait::async_trait;
use byteorder::LE;
use futures_channel::oneshot;
use futures_util::{FutureExt, StreamExt};
use gettextrs::gettext;
use gtk::gio;
use gtk::gio::prelude::*;
use gtk::glib;
use gtk::prelude::*;
use zbus::zvariant::{from_slice, EncodingContext};

use crate::Action;

pub enum FileChooserAction {
    OpenFile(
        String,
        WindowIdentifierType,
        OpenFileOptions,
        oneshot::Sender<Response<OpenFileResults>>,
    ),
}

pub struct FileChooser {
    sender: glib::Sender<Action>,
}

impl FileChooser {
    pub fn new(sender: glib::Sender<Action>) -> Self {
        Self { sender }
    }
}

#[async_trait]
impl RequestImpl for FileChooser {
    async fn close(&self) {
        log::debug!("IN Close()");
    }
}

#[async_trait]
impl FileChooserImpl for FileChooser {
    async fn open_file(
        &self,
        app_id: AppID,
        window_identifier: WindowIdentifierType,
        title: &str,
        options: OpenFileOptions,
    ) -> Response<OpenFileResults> {
        log::debug!("IN OpenFile({app_id}, {window_identifier}, {title}, {options:?}");

        let (sender, receiver) = oneshot::channel::<Response<OpenFileResults>>();

        let title = title.to_owned();

        self.sender
            .send(Action::FileChooser(FileChooserAction::OpenFile(
                title,
                window_identifier,
                options,
                sender,
            )))
            .unwrap();

        let mut stream = receiver.into_stream();
        let response = stream.next().await.unwrap().unwrap();
        log::debug!("OUT OpenFile({response:?})");

        response
    }

    async fn save_file(
        &self,
        _app_id: AppID,
        _window_identifier: WindowIdentifierType,
        _title: &str,
        _options: SaveFileOptions,
    ) -> Response<SaveFileResults> {
        todo!();
    }

    async fn save_files(
        &self,
        _app_id: AppID,
        _window_identifier: WindowIdentifierType,
        _title: &str,
        _options: SaveFilesOptions,
    ) -> Response<SaveFilesResults> {
        todo!();
    }
}

pub fn handle_action(action: FileChooserAction) -> glib::Continue {
    match action {
        FileChooserAction::OpenFile(title, window_identifier, options, sender) => {
            let ctx = glib::MainContext::default();
            ctx.spawn_local(async move {
                if let Ok(res) = open_file_dialog(&title, window_identifier, options).await {
                    sender.send(Response::ok(res)).unwrap();
                } else {
                    sender.send(Response::other()).unwrap();
                }
            });

            glib::Continue(true)
        }
    }
}

// FIXME Use https://github.com/gtk-rs/gtk4-rs/pull/1234 once it is in
fn dialog_add_choice<D: glib::IsA<gtk::FileChooser>>(
    dialog: &D,
    id: &str,
    label: &str,
    options: &[(&str, &str)],
) {
    use gtk::glib::translate::*;
    unsafe {
        let (options_ids, options_labels) = if options.is_empty() {
            (std::ptr::null(), std::ptr::null())
        } else {
            let stashes_ids = options
                .iter()
                .map(|o| o.0.to_glib_none())
                .collect::<Vec<_>>();
            let stashes_labels = options
                .iter()
                .map(|o| o.1.to_glib_none())
                .collect::<Vec<_>>();
            (
                stashes_ids
                    .iter()
                    .map(|o| o.0)
                    .collect::<Vec<*const libc::c_char>>()
                    .as_ptr(),
                stashes_labels
                    .iter()
                    .map(|o| o.0)
                    .collect::<Vec<*const libc::c_char>>()
                    .as_ptr(),
            )
        };

        gtk::ffi::gtk_file_chooser_add_choice(
            dialog.as_ref().to_glib_none().0,
            id.to_glib_none().0,
            label.to_glib_none().0,
            mut_override(options_ids),
            mut_override(options_labels),
        );
    }
}

async fn open_file_dialog(
    title: &str,
    window_identifier: WindowIdentifierType,
    options: OpenFileOptions,
) -> anyhow::Result<OpenFileResults> {
    log::debug!("Spawning a dialog with identifier: {window_identifier}");

    let multiple = options.multiple == Some(true);
    let is_directory = options.directory == Some(true);

    let mut choices = options.choices.clone().unwrap_or_default();

    let accept_label = if multiple {
        options.accept_label.unwrap_or_else(|| gettext("_Select"))
    } else {
        options.accept_label.unwrap_or_else(|| gettext("_Open"))
    };

    let action = if is_directory {
        gtk::FileChooserAction::SelectFolder
    } else {
        gtk::FileChooserAction::Open
    };

    let file_chooser = gtk::FileChooserDialog::new(
        Some(title),
        gtk::Window::NONE,
        action,
        &[
            (&gettext("Cancel"), gtk::ResponseType::Cancel),
            (&accept_label, gtk::ResponseType::Ok),
        ],
    );

    file_chooser.set_default_response(gtk::ResponseType::Ok);

    if let Some(filters) = options.filters {
        for filter in filters {
            let gfilter = gtk::FileFilter::new();

            let label = filter.label();
            if !label.is_empty() {
                gfilter.set_name(Some(filter.label()));
            }

            for mime in filter.mimetype_filters() {
                gfilter.add_mime_type(mime);
            }

            for pattern in filter.pattern_filters() {
                gfilter.add_pattern(pattern);
            }

            file_chooser.add_filter(&gfilter);

            if Some(filter) == options.current_filter {
                file_chooser.set_filter(&gfilter);
            }
        }
    }

    let modal = Some(true) == options.modal;
    file_chooser.set_modal(modal);

    let choice_label = if is_directory {
        gettext("Open directories read-only")
    } else {
        gettext("Open files read-only")
    };
    choices.push(Choice::boolean("read-only", &choice_label, false));
    for choice in choices.iter() {
        let id = choice.id();
        let label = choice.label();
        let pairs = choice.pairs();
        let initial_selection = choice.initial_selection();
        dialog_add_choice(&file_chooser, id, label, &pairs);
        file_chooser.set_choice(id, initial_selection);
    }

    crate::external_window::set_foreign_parent(&file_chooser, window_identifier);

    let result = file_chooser.run_future().await;

    if result == gtk::ResponseType::Ok {
        let files = file_chooser.files();
        let uris: Vec<url::Url> = files
            .into_iter()
            .filter_map(Result::ok)
            .map(|file| file.downcast::<gio::File>().unwrap().uri())
            .filter_map(|x| url::Url::parse(&x).ok())
            .collect();

        let uris = if uris.is_empty() { None } else { Some(uris) };

        let current_filter = file_chooser.filter().and_then(|gfilter| {
            let variant = gfilter.to_gvariant();
            let ctxt = EncodingContext::<LE>::new_gvariant(0);
            let decoded: zbus::zvariant::Result<FileFilter> = from_slice(variant.data(), ctxt);

            decoded.ok()
        });

        // We get the values for the choices.
        let choices = choices
            .iter()
            .filter_map(|choice| {
                file_chooser.choice(choice.id()).map(|initial_selection| {
                    Choice::new(choice.id(), choice.label(), &initial_selection)
                })
            })
            .collect::<Vec<Choice>>();

        let writable = file_chooser.choice("read-only").map(|val| &val == "false");
        log::debug!("writable: {writable:?}");

        let res = OpenFileResults {
            uris,
            writable,
            current_filter,
            choices: Some(choices),
        };

        Ok(res)
    } else {
        anyhow::bail!("Dialog closed")
    }
}
