Name:       xdp-ashpd-gnome
Version:    1.0.0
Release:    2%{?dist}
Summary:    ASHPD Backend demo
License:    GPLv3+
URL:        https://gitlab.gnome.org/msandova/xdp-ashpd-gnome

Source0:     ashpd-gnome.portal

%description
Portal backend demo

%prep
%install
install -Dpm 644 %{SOURCE0} %{buildroot}/%{_datadir}/xdg-desktop-portal/portals/ashpd-gnome.portal

%files
%{_datadir}/xdg-desktop-portal/portals/ashpd-gnome.portal

%changelog
* Thu Jan 05 2023 Maximiliano Sandoval <msandova@protonmail.com> - 1.0.0-1
- Initial spec
